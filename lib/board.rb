class Board
  attr_reader :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def [](position)
    row, col = position
    @grid[row][col]
  end

  def []=(position, val)
    row, col = position
    @grid[row][col] = val
  end

  def count
    ship_count = 0
    @grid.flatten.count { |ele| ship_count += 1 if ele == :s }
    ship_count
  end

  def empty?(position = nil)
    if position
      self[position] == nil ? true : false
    else
      @grid.flatten.none? { |ele| ele == :s }
    end
  end

  def full?
    @grid.flatten.all? { |ele| ele == :s }
  end

  def place_random_ship
    raise "Board is full!!!" if full?
    random_pos = []
    random_pos << rand(0...@grid.length) until random_pos.length == 2
    self[random_pos] = :s unless self[random_pos] != nil
  end

  def won?
    empty? ? true : false
  end
end
